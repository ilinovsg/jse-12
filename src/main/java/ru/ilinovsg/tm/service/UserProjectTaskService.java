package ru.ilinovsg.tm.service;

import ru.ilinovsg.tm.entity.Project;
import ru.ilinovsg.tm.entity.Task;
import ru.ilinovsg.tm.entity.User;
import ru.ilinovsg.tm.repository.ProjectRepository;
import ru.ilinovsg.tm.repository.TaskRepository;
import ru.ilinovsg.tm.repository.UserRepository;

import java.util.Collections;
import java.util.List;

public class UserProjectTaskService {

    private final UserRepository userRepository;

    private final ProjectRepository projectRepository;

    private final TaskRepository taskRepository;

    public UserProjectTaskService(UserRepository userRepository, ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.userRepository = userRepository;
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public List<Project> findAllProjectsByUserId(final Long userId) {
        if (userId == null) return Collections.emptyList();
        return projectRepository.findAllByUserId(userId);
    }

    public List<Task> findAllTasksByUserId(final Long userId) {
        if (userId == null) return Collections.emptyList();
        return taskRepository.findAllByUserId(userId);
    }

    public Task addTaskToUser (final Long userId, final Long taskId) {
        final User user = userRepository.findById(userId);
        if (user == null) return null;
        final Task task = taskRepository.findById(taskId);
        if (task == null) return null;
        task.setUserId(userId);
        return task;
    }

    public Task removeTaskFromUser (final Long userId, final Long taskId) {
        final Task task = taskRepository.findByUserIdAndId(userId, taskId);
        if (task == null) return null;
        task.setUserId(null);
        return task;
    }

    public Project addProjectToUser (final Long userId, final Long projectId) {
        final User user = userRepository.findById(userId);
        if (user == null) return null;
        final Project project = projectRepository.findById(projectId);
        if (project == null) return null;
        project.setUserId(userId);
        return project;
    }

    public Project removeProjectFromUser (final Long userId, final Long projectId) {
        final Project project = projectRepository.findByUserIdAndId(userId, projectId);
        if (project == null) return null;
        project.setUserId(null);
        return project;
    }

}
